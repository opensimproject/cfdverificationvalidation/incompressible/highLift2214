// Create wing based on NACA airfoil

Include "cleanAirfoil.geo" ;
mll = newll ;
Line Loop(mll) = foil_Splines[] ;

// Include "flap.geo" ;
// fll = newll ;
// Line Loop(fll) = foil_Splines[] ;

ms = news ;
Plane Surface(ms) = {mll};

// fs = news ;
// Plane Surface(fs) = {fll};

Translate {0, 0, -0.5} {
//   Surface{fs,ms};
  Surface{ms};
}

Extrude {0, 0, 1.0} {
//   Surface{fs,ms};
  Surface{ms};
  Layers{1};
  Recombine;
}


