// ++++++++++++++++++++++++++++++++++++++++++++++++//
// Background block mesh for 2D external aero
// ++++++++++++++++++++++++++++++++++++++++++++++++//

//    x-----------x-------x-----------x
// 13 |        14 |    15 |        16 |  
//    |           |       |           |
//    |           |       |           |
//    |           |       |           |
//    x-----------x-------x-----------x
//  9 |        10 |    11 |        12 |  
//    |           |       |           |
//    |           |       |           |
//    x-----------x-------x-----------x
//  5 |         6 |     7 |         8 |  
//    |           |       |           |
//    |           |       |           |
//    |           |       |           |
//    x-----------x-------x-----------x
//  1           2       3           4   

// ++++++++++++++++++++++++++++++++++++++++++++++++//

// Domain centre
cX = 0.3;
cY = 0;
cZ = 0;

// Snap domain size
sdX = 1.5;
sdY = 0.75;
sdZ = 1;  // Not used in 2D

// Farfield domain size
fdUwX = 10;
fdDwX = 10;
fdUwY = 10;
fdDwY = 10;
fdZ = 1; // Not used in 2D

// No cells snap domain
sdNoCellX = 500;
sdNoCellY = 250;
sdNDoCellZ = 1;

// No cells farfield domain
fdNoCellX = 10;
fdNoCellY = 10;
// fdNoCellZ = 1;


// 2D extrusion height
extHeight = sdX/sdNoCellX;

// Characteristic length (Not used in structured meshes
cL = 1;
cLS = 0.002;


Point(1) = {cX-fdUwX  ,cY-fdUwY,0,cL};
Point(2) = {cX-0.5*sdX,cY-fdUwY,0,cL};
Point(3) = {cX+0.5*sdX,cY-fdUwY,0,cL};
Point(4) = {cX+fdDwX  ,cY-fdUwY,0,cL};

Point(5) = {cX-fdUwX  ,cY-0.5*sdY,0,cL};
Point(6) = {cX-0.5*sdX,cY-0.5*sdY,0,cLS};
Point(7) = {cX+0.5*sdX,cY-0.5*sdY,0,cLS};
Point(8) = {cX+fdDwX  ,cY-0.5*sdY,0,cL};

Point(9)  = {cX-fdUwX  ,cY+0.5*sdY,0,cL};
Point(10) = {cX-0.5*sdX,cY+0.5*sdY,0,cLS};
Point(11) = {cX+0.5*sdX,cY+0.5*sdY,0,cLS};
Point(12) = {cX+fdDwX  ,cY+0.5*sdY,0,cL};

Point(13) = {cX-fdUwX  ,cY+fdDwY,0,cL};
Point(14) = {cX-0.5*sdX,cY+fdDwY,0,cL};
Point(15) = {cX+0.5*sdX,cY+fdDwY,0,cL};
Point(16) = {cX+fdDwX  ,cY+fdDwY,0,cL};

// Horizontal lines
Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {5, 6};
Line(5) = {6, 7};
Line(6) = {7, 8};
Line(7) = {9, 10};
Line(8) = {10, 11};
Line(9) = {11, 12};
Line(10) = {13, 14};
Line(11) = {14, 15};
Line(12) = {15, 16};

// Vertical lines
Line(14) = {1, 5};
Line(15) = {5, 9};
Line(16) = {9, 13};
Line(17) = {2, 6};
Line(18) = {6, 10};
Line(19) = {10, 14};
Line(20) = {3, 7};
Line(21) = {7, 11};
Line(22) = {11, 15};
Line(23) = {4, 8};
Line(24) = {8, 12};
Line(25) = {12, 16};

Line Loop(26) = {1, 17, -4, -14};
Plane Surface(27) = {26};
Line Loop(28) = {17, 5, -20, -2};
Plane Surface(29) = {28};
Line Loop(30) = {20, 6, -23, -3};
Plane Surface(31) = {30};
Line Loop(32) = {15, 7, -18, -4};
Plane Surface(33) = {32};
Line Loop(34) = {18, 8, -21, -5};
Plane Surface(35) = {34};
Line Loop(36) = {21, 9, -24, -6};
Plane Surface(37) = {36};
Line Loop(38) = {16, 10, -19, -7};
Plane Surface(39) = {38};
Line Loop(40) = {19, 11, -22, -8};
Plane Surface(41) = {40};
Line Loop(42) = {12, -25, -9, 22};
Plane Surface(43) = {42};

// Structured mesh
// Transfinite Line {1, 4, 7, 10} = fdNoCellX Using Progression 0.7;
// Transfinite Line {2, 5, 8, 11} = sdNoCellX Using Progression 1;
// Transfinite Line {3, 6, 9, 12} = fdNoCellX Using Progression 1.42857142857;
// Transfinite Line {14, 17, 20, 23} = fdNoCellY Using Progression 0.7;
// Transfinite Line {15, 18, 21, 24} = sdNoCellY Using Progression 1;
// Transfinite Line {16, 19, 22, 25} = fdNoCellY Using Progression 1.42857142857;

Transfinite Line {5, 8} = sdNoCellX Using Progression 1;
Transfinite Line {18, 21} = sdNoCellY Using Progression 1;

// Transfinite Surface {27};
// Transfinite Surface {29};
// Transfinite Surface {31};
// Transfinite Surface {33};
Transfinite Surface {35};
// Transfinite Surface {37};
// Transfinite Surface {39};
// Transfinite Surface {41};
// Transfinite Surface {43};

Recombine Surface {27};
Recombine Surface {29};
Recombine Surface {31};
Recombine Surface {33};
Recombine Surface {35};
Recombine Surface {37};
Recombine Surface {39};
Recombine Surface {41};
Recombine Surface {43};

// Middle domain
Extrude {0, 0, extHeight} {
  Surface{27, 29, 31, 33, 35, 37, 39, 41, 43};
  Layers{sdNDoCellZ}; 
  Recombine;
}

// Patches
Physical Surface("inlet") = {64, 118, 184};
Physical Surface("outlet") = {104, 170, 232};
Physical Surface("topAndBottom") = {52, 86, 108, 188, 210, 228};
Physical Surface("tempFront") = {65, 87, 109, 131, 153, 175, 197, 219, 241};
Physical Surface("tempBack") = {27, 29, 31, 33, 35, 37, 39, 41, 43};
Physical Volume("internal") = {1, 2, 3, 4, 5, 6, 7, 8, 9};
