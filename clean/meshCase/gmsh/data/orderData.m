% *****************************************
% Reorder and append data
% *****************************************

% Read data ...............................
s1=load('closedAirfoilUpper.dat');
xU = s1(:,1);   
yU = s1(:,2);   

s2=load('closedAirfoilLower.dat');
xL = s2(:,1);   
yL = s2(:,2);   

% Write reordered data .................... 
[stat, err, msg] = stat ('closedAirfoil.dat');
fid = fopen('closedAirfoil.dat','w');

for j = 1:length(xU)
  fwrite (fid,[num2str(xU(length(xL)+1-j)),'  ',num2str(yU(length(xL)+1-j))]);
  fprintf(fid,' \n');
end

for j = 1:length(xL)
  fwrite (fid,[num2str(xL(j)),'  ',num2str(yL(j))]);
  fprintf(fid,' \n');
end
fclose (fid);
