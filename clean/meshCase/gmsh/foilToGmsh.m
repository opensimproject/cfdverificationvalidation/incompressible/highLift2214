% ...................................................................%
% Convert dat file with airfoil points
% to gmsh format
% ...................................................................%

% Version ...........................................................%

% 1.0 - Created by (JH - June 2013)
% 1.1 - Fixed splines for smooth surfaces (MC - June 2014)


% Conversion factor .................................................%
cF=0.0061; 		% NOTE: Coordinates must be in METER

foil=('cleanAirfoil');      % NOTE: Specify the data file name <fileName>.txt
inFile = strcat(foil,'.dat');
outFile = strcat(foil,'.geo');

%.............................. NOTE ................................%
% The ordering of the points in the input .txt file is important.
% They should be ordered continuously, starting at the TE, and moving 
% around to the LE, then back to the TE.

% If the first and last points in the file have the same coordinates,
% the airfoil is assumed to have a SHARP TE. Otherwise, the TE is
% assumed to be BLUNT.
%....................................................................%

isBluntTE = 0;          % True (1) if blunt TE / False (0) if sharp TE

s = load(inFile);
x = cF*s(:,1);          % NOTE: May depend on data file
y = cF*s(:,2);          % NOTE: May depend on data file
%  y = cF*s(:,3);          % NOTE: May depend on data file

% Charateristic length
len_le = 0.002;
len_mp = 0.002;
len_te = 0.002;

% Number of points
noPoints=length(x);

[stat, err, msg] = stat (outFile);
fid = fopen(outFile,'w');  % Write file 
if (err != 0)      
  disp ("Writing foil.geo");
endif
fwrite (fid,'// Gmsh file for foil ');
fprintf(fid,' \n \n');

%  fwrite (fid,['    ',num2str(fAmax),'    ',num2str(fRmax)]);
%  fprintf(fid,' \n');

fwrite (fid,['foil_Points[] = {} ;']);
fprintf(fid,' \n');
fwrite (fid,['foil_Splines[] = {} ;']);
fprintf(fid,' \n\n');

% Write the points
fwrite (fid,['// Points ..............................   ']);
fprintf(fid,' \n');
for i=1:noPoints
  L2 = (x(i) - min(x))/(max(x) - min(x));
  L1 = 1-L2;
    
  foil_len = len_le*L1*(2*L1-1) + ...
	      len_mp*4*L1*L2 + ...
	      len_te*L2*(2*L2-1) ;

  if ((i>1) && ( (y(i) == y(1)) && (x(i) == x(1)) )) && (i ~= noPoints)
    noPoints = noPoints-1;
    disp ('***********************************************************************');
    disp (['NOTE: Point ',num2str(i-1),' has the same coordinates as point 0 and is not included']);
    disp ('***********************************************************************');
    fprintf(fid,' \n\n');
  elseif (i == noPoints) && ( (y(i) == y(1)) && (x(i) == x(1)) )
    noPoints = noPoints-1;
    disp ('***********************************************************************');
    disp (['NOTE: Point ',num2str(i-1),' has the same coordinates as point 0 and is not included']);
    disp (['NOTE: Last point in file was identical to the first point: assuming sharp TE']);
    disp ('***********************************************************************');
    fprintf(fid,' \n\n');
    % Update isBluntTE to reflect that the airfoil has a sharp TE
    isBluntTE = 0;
  else
    fwrite (fid,['p = newp ;']);
    fprintf(fid,' \n');
    fwrite (fid,['Point(p) = {',num2str(x(i)),',',num2str(y(i)),',0,',num2str(foil_len),'} ;']);
    fprintf(fid,' \n');
    fwrite (fid,['foil_Points[',num2str(i-1),'] = p ;']);
    fprintf(fid,' \n');
  end

  
  
  
end

% Write the splines

%.............................. NOTE ................................%
% An airfoil with a SHARP TE will only have one, continuous spline.
% The spline from point (i=1) to point (i=noPoints) is self-closing.

% An airfoil with a BLUNT TE will a 2-point spline (straight line) to
% model the TE; it will also have a continuous spline for the rest of
% the airfoil surface.
% The spline from point (i=1) to point (i=noPoints) is OPEN, and
% represents the surface without the blunt TE connector.
%....................................................................%

% Write the surface spline ..........................................
% If blunt TE, write the TE spline
if isBluntTE
  fprintf(fid,' \n');
  fwrite (fid,['// Splines .............................   ']);
  fprintf(fid,' \n');
  fwrite (fid,['c = newc ;']);
  fprintf(fid,' \n');
  fwrite (fid,['Spline(c) = foil_Points[{0:',num2str(noPoints-1),'}] ;']); % surface spline
  fprintf(fid,' \n');
  fwrite (fid,['foil_Splines[0] = c ;']);
  fprintf(fid,' \n');
  fwrite (fid,['c = newc ;']);
  fprintf(fid,' \n');
  fwrite (fid,['Spline(c) = foil_Points[{',num2str(noPoints-1),',0}] ;']); % TE spline
  fprintf(fid,' \n');
  fwrite (fid,['foil_Splines[1] = c ;']);
  fprintf(fid,' \n');
else
  fprintf(fid,' \n');
  fwrite (fid,['// Splines .............................   ']);
  fprintf(fid,' \n');
  fwrite (fid,['c = newc ;']);
  fprintf(fid,' \n');
  fwrite (fid,['Spline(c) = foil_Points[{0:',num2str(noPoints-1),'}] ;']); % surface spline
  fprintf(fid,' \n');
  fwrite (fid,['foil_Splines[0] = c ;']);
  fprintf(fid,' \n');
  fwrite (fid,['c = newc ;']);
  fprintf(fid,' \n');
  fwrite (fid,['Spline(c) = foil_Points[{',num2str(noPoints-1),',0}] ;']); % straight line at last point
  fprintf(fid,' \n');
  fwrite (fid,['foil_Splines[1] = c ;']);
  fprintf(fid,' \n');
end



%  c = newc ; i = 2*NACA4_nspl+NACA4_pps ;
%  Spline(c) = {NACA4_Points[{i*(NACA4_pps-1):(2*NACA4_npts-1)}],
%  	     NACA4_Points[0]} ;

fclose (fid);

