// ++++++++++++++++++++++++++++++++++++++++++++++++//
// Background o mesh for 2D external aero
// ++++++++++++++++++++++++++++++++++++++++++++++++//

//           ---------x---------------x
//         --        9             10 |  
//       -                            |
//      -                             |
//     -                              |
//    -           x-------x           |
//    -         7 |     8 |           |  
//    x           |   x   |           |
//  4 -           | 1     |           |
//    -           x-------x           |
//     -        5      6              |  
//      -                             |
//       -                            |
//         --                         |
//           ---------x---------------x
//                  2               3

// ++++++++++++++++++++++++++++++++++++++++++++++++//

// Domain centre
cX = 0.3;
cY = -0.05;
cZ = 0;

// Snap domain size
sdX = 0.9;
sdY = 0.45;
sdZ = 1;  // Not used in 2D

// Farfield domain size 
fdUwX = 90;  // 60 -> 100 x chord
fdDwX = 90;
fdUwY = 90;
fdDwY = 90;
fdZ = 1; // Not used in 2D

// No cells snap domain
sdNoCellX = 440;
sdNoCellY = 220;
sdNDoCellZ = 1;

// 2D extrusion height
extHeight = sdX/sdNoCellX;

// Characteristic length 
cL  = 4;      // Farfield (If made to large gmsh creates weird defauls faces)
cLM = 0.05;  // Next to box
cLS = 0.003;  // Next to box


Point(1) = {cX  ,cY ,0,cL};
Point(2) = {cX  ,cY-fdUwY,0,cL};
Point(3) = {cX+fdDwX  ,cY-fdUwY,0,cL};
Point(4) = {cX-fdUwX  ,cY,0,cL};
Point(5) = {cX-0.5*sdX,cY-0.5*sdY,0,cLS};
Point(6) = {cX+0.5*sdX,cY-0.5*sdY,0,cLS};
Point(7) = {cX-0.5*sdX,cY+0.5*sdY,0,cLS};
Point(8) = {cX+0.5*sdX,cY+0.5*sdY,0,cLS};
Point(9) = {cX,cY+fdDwY,0,cL};
Point(10) = {cX+fdDwX  ,cY+fdDwY,0,cL};

Point(11) = {cX-1.0*sdX,cY-1.5*sdY,0,cLM};
Point(12) = {cX+1.0*sdX,cY-1.5*sdY,0,cLM};
Point(13) = {cX-1.0*sdX,cY+1.5*sdY,0,cLM};
Point(14) = {cX+1.0*sdX,cY+1.5*sdY,0,cLM};

Circle(1) = {9, 1, 4};
Circle(2) = {4, 1, 2};
Line(3) = {2, 3};
Line(4) = {3, 10};
Line(5) = {10, 9};
Line(6) = {5, 6};
Line(7) = {6, 8};
Line(8) = {8, 7};
Line(9) = {7, 5};

Line(10) = {11, 12};
Line(11) = {12, 14};
Line(12) = {14, 13};
Line(13) = {13, 11};
Line(14) = {11, 5};
Line(15) = {6, 12};
Line(16) = {8, 14};
Line(17) = {7, 13};
Line(18) = {2, 11};
Line(19) = {12, 3};
Line(20) = {14, 10};
Line(21) = {13, 9};

Line Loop(22) = {2, 18, -13, 21, 1};
Plane Surface(23) = {22};
Line Loop(24) = {18, 10, 19, -3};
Plane Surface(25) = {24};
Line Loop(26) = {19, 4, -20, -11};
Plane Surface(27) = {26};
Line Loop(28) = {20, 5, -21, -12};
Plane Surface(29) = {28};
Line Loop(30) = {10, -15, -6, -14};
Plane Surface(31) = {30};
Line Loop(32) = {11, -16, -7, 15};
Plane Surface(33) = {32};
Line Loop(34) = {16, 12, -17, -8};
Plane Surface(35) = {34};
Line Loop(36) = {17, 13, 14, -9};
Plane Surface(37) = {36};
Line Loop(38) = {6, 7, 8, 9};
Plane Surface(39) = {38};

Transfinite Line {6, 8} = sdNoCellX Using Progression 1;
Transfinite Line {9, 7} = sdNoCellY Using Progression 1;
Transfinite Surface {39};
Recombine Surface {39};

// Recombine Surface {23};
// Recombine Surface {25};
// Recombine Surface {27};
// Recombine Surface {29};
// Recombine Surface {31};
// Recombine Surface {33};
// Recombine Surface {35};
// Recombine Surface {37};

// Extrude {0, 0, 1} {
Extrude {0, 0, extHeight} {
  Surface{29, 23, 25, 27, 33, 35, 37, 31, 39};
  Layers{sdNDoCellZ}; 
  Recombine;
}

Physical Volume("internal") = {2, 6, 7, 9, 5, 8, 3, 4, 1};

Physical Surface("tempFront") = {88, 110, 132, 61, 154, 220, 198, 242, 176};
Physical Surface("tempBack") = {23, 25, 29, 27, 75, 37, 35, 31, 33, 39};
Physical Surface("outlet") = {123};
Physical Surface("inlet") = {87, 71};
Physical Surface("topAndBottom") = {52, 109};
