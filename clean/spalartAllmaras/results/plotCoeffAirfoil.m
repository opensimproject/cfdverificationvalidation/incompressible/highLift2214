% *****************************************
% Plot the pressure coefficient along the 
% coord of the airfoil
% *****************************************

% Computation of cp
% Cp = (P - P_inf) / (1/2 * rho_inf * V_inf ^ 2)

% Convertion factor 
conF=0.61; 		% NOTE: Coordinates must be in METER

% Input variables
pInf = 0; % 101325;
TInf = 288.15;
M    = 0.2;
R    = 287.05;
gamma = 1.4;

c = (gamma*R*TInf)^0.5;
uInf = M*c
rhoInf = gamma*pInf/(c*c);


% Read numerical results ..................
%sP=load('../postProcessing/surfaces/10000/p_walls_constant.raw');
sP=load('p_walls_constant.raw');
x = sP(:,1);   
y = sP(:,2);   
z = sP(:,3);   
pNum = sP(:,4);   

noNodes = length(x);
L 	= 1;

% Compute cp ..............................
%  cpNum = -(pNum - pInf)/(0.5*rhoInf*uInf*uInf);
cpNum = -(pNum - pInf)/(0.5*uInf*uInf); % Incompressible solver compute p/rho


% Distinguish between main and flap .......
cF = 1;
cM = 1;

%for i=1:noNodes
%  if (y(i) < 0) && (x(i) >0.275)  % flap
%    xF(cF) = x(i);
%    yF(cF) = y(i);
%    zF(cF) = z(i);
%    pF(cF) = pNum(i);
%    cpF(cF) = cpNum(i);
%    cF = cF+1;
%  else				% main
%    xM(cM) = x(i);
%    yM(cM) = y(i);
%    zM(cM) = z(i);
%    pM(cM) = pNum(i);
%    cpM(cM) = cpNum(i);
%    cM = cM+1;
%  end;
%end;
  
  [stat, err, msg] = stat ('pressCoeff.out');
  fid = fopen('pressCoeff.out','a');
  if (err != 0)      
  fwrite (fid,'# X      Y      Z      CP');
    fprintf(fid,' \n');
  endif

  for i=1:noNodes
    fwrite (fid,[num2str(x(i)),'    ',num2str(y(i)),'    ',num2str(z(i)),'    ',num2str(cpNum(i))]);
    fprintf(fid,' \n');
  end;

