echo "
% *****************************************
% Plot the lift and drag coefficients  
% *****************************************
  

% Read experimental results ...............
sE=load('expData/cleanAoACl.txt');
eAoA = sE(:,1);   
eCl = sE(:,2);   

sE=load('expData/cleanCmCl.txt');
eCmCl = sE(:,2);
eCm = sE(:,1);   

sE=load('expData/cleanCdCl.txt');
eCdCl = sE(:,2);
eCd = sE(:,1);   

% Interpolate experimental data 
% eCmI = interp1(eCmCl,eCm,eCl);
% eCdI = interp1(eCdCl,eCd,eCl);


% Read numerical results ...............
sN=load('forceCoeff.out');
nAoA = sN(:,1);   
%  nCl = sN(:,2);   
nCl = sN(:,2);
nCd = -sN(:,3);
nCm = sN(:,4);


% Plot coefficients .......................
h=figure (1);
hold on
plot(eAoA,eCl,'k+','linewidth',2,'markersize',4)
plot(nAoA,nCl,'b^-','linewidth',2,'markersize',4)
%  plot(eAoa,0.97*eCl,'k-.','linewidth',2,'markersize',4)

%  grid on;
FN = findall(h,'-property','FontName');
set(FN,'FontName','/usr/share/fonts/dejavu/DejaVuSerifCondensed.ttf');
FS = findall(h,'-property','FontSize');
set(FS,'FontSize',6);
ylabel('Lift coefficient, c_l','FontSize',8)
xlabel('Angle of attack, \alpha','FontSize',8)

L=legend('Experimental ','simpleFoam ',0);
FL1= findall(L,'-property','FontName');
FL2= findall(L,'-property','FontSize');
set(FL1,'FontName','/usr/share/fonts/dejavu/DejaVuSerifCondensed.ttf');
set(FL2,'FontSize',6);

H = 3; W = 3;
set(h,'PaperUnits','inches')
set(h,'PaperOrientation','portrait');
set(h,'PaperSize',[H,W])
set(h,'PaperPosition',[0,0,W,H])

axis([-7 16 -0.8 2.0]);

print(h,'-dpng','-color','clAlpha.png');


h=figure (2);
hold on
% plot(eCl,eCmI,'k+','linewidth',2,'markersize',4)
% plot(nCl,nCm,'b^-','linewidth',2,'markersize',4)
% plot(eCmI,eCl,'k+','linewidth',2,'markersize',4)
plot(eCm,eCmCl,'k+','linewidth',2,'markersize',4)
plot(nCm,nCl,'b^-','linewidth',2,'markersize',4)

%  grid on;
FN = findall(h,'-property','FontName');
set(FN,'FontName','/usr/share/fonts/dejavu/DejaVuSerifCondensed.ttf');
FS = findall(h,'-property','FontSize');
set(FS,'FontSize',6);
ylabel('Lift coefficient, c_l','FontSize',8)
xlabel('Moment of pitch, c_m','FontSize',8)

L=legend('Experimental ','simpleFoam ',0);
FL1= findall(L,'-property','FontName');
FL2= findall(L,'-property','FontSize');
set(FL1,'FontName','/usr/share/fonts/dejavu/DejaVuSerifCondensed.ttf');
set(FL2,'FontSize',6);

H = 3; W = 3;
set(h,'PaperUnits','inches')
set(h,'PaperOrientation','portrait');
set(h,'PaperSize',[H,W])
set(h,'PaperPosition',[0,0,0.6*W,H])

axis([0 0.1 -0.8 2.0]);

print(h,'-dpng','-color','cmcl.png');



h=figure (3);
hold on
% plot(eCl,eCdI,'k+','linewidth',2,'markersize',4)
% plot(nCl,nCd-nCd(1),'b^-','linewidth',2,'markersize',4)
% plot(eCdI,eCl,'k+','linewidth',2,'markersize',4)
plot(eCd,eCdCl,'k+','linewidth',2,'markersize',4)
plot(nCd,nCl,'b^-','linewidth',2,'markersize',4)

%  grid on;
FN = findall(h,'-property','FontName');
set(FN,'FontName','/usr/share/fonts/dejavu/DejaVuSerifCondensed.ttf');
FS = findall(h,'-property','FontSize');
set(FS,'FontSize',6);
ylabel('Lift coefficient, c_l','FontSize',8)
xlabel('Drag coefficient, c_d','FontSize',8)

L=legend('Experimental ','simpleFoam ',0);
FL1= findall(L,'-property','FontName');
FL2= findall(L,'-property','FontSize');
set(FL1,'FontName','/usr/share/fonts/dejavu/DejaVuSerifCondensed.ttf');
set(FL2,'FontSize',6);

H = 3; W = 3;
set(h,'PaperUnits','inches')
set(h,'PaperOrientation','portrait');
set(h,'PaperSize',[H,W])
set(h,'PaperPosition',[0,0,0.8*W,H])

axis([0 0.05 -0.8 2.0]);

print(h,'-dpng','-color','cdcl.png');


" > plotCoeffExp.m

octave plotCoeffExp.m
