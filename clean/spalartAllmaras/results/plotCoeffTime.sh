echo "
  % *****************************************
  % Plot the drag and lift coefficients over
  % time to determine if convergence is 
  % reached
  % *****************************************


  % Read numerical results ..................
  s=load('forceCoeffs.dat');
  t = s(:,1);   
  cm = s(:,2);   
  cd = s(:,3);   
  cl = s(:,4);   

  noNodes = length(t);
  L 	= 1;

  [stat, err, msg] = stat ('forceCoeff.out');
  fid = fopen('forceCoeff.out','a');
  if (err != 0)      
  fwrite (fid,'# AoA  cl              cd             cm');
    fprintf(fid,' \n');
  endif

  fwrite (fid,[num2str($AOA),'      ',num2str(mean(cl(noNodes-200:noNodes))),'      ',num2str(mean(cd(noNodes-50:noNodes))),'      ',num2str(mean(cm(noNodes-200:noNodes)))]);
  fprintf(fid,' \n');


  % Plot coefficients over time .............
  h=figure (1);
  hold on
  %plot(t,cm,'b-','linewidth',2,'markersize',4)
  plot(t,cd,'k-','linewidth',2,'markersize',4)
  %plot(t,cl,'g-','linewidth',2,'markersize',4)


  %  grid on;
  FN = findall(h,'-property','FontName');
  set(FN,'FontName','/usr/share/fonts/dejavu/DejaVuSerifCondensed.ttf');
  FS = findall(h,'-property','FontSize');
  set(FS,'FontSize',6);
  %  ylabel('Drag coefficient, c_d ','FontSize',8)
  ylabel('Force coefficients','FontSize',8)
  xlabel('Time, t (s)','FontSize',8)

  L=legend('Pitch moment ','Drag ','Lift ',1);
  FL1= findall(L,'-property','FontName');
  FL2= findall(L,'-property','FontSize');
  set(FL1,'FontName','/usr/share/fonts/dejavu/DejaVuSerifCondensed.ttf');
  set(FL2,'FontSize',8);

  H = 3; W = 4;
  set(h,'PaperUnits','inches')
  set(h,'PaperOrientation','portrait');
  set(h,'PaperSize',[H,W])
  set(h,'PaperPosition',[0,0,W,H])

  %axis([0 max(t) -2 2]);
  axis([0 max(t) -0.1 0.05]);

  print(h,'-dpng','-color','forceCoeff.png');
" > plotCoeffTime.m

octave plotCoeffTime.m
