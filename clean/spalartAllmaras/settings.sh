# Files
SOURCE_DIR=airfoil
CONFIG_FILE=runs.txt
RUN_SCRIPT=runCase

# Number of processors per run
PROC=4
# Cluster nodes		
# NODE=babbel
EXNODE=babbel,doffel,bekkie,spongebob
# Priority of run
PRIOR=contract #academic/contract/priority;

# Variables
PI=3.14159265359
