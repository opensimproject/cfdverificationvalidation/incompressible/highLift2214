/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  2.2.0                                 |
|   \\  /    A nd           | Web:      www.OpenFOAM.org                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    object      snappyHexMeshDict;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

// Which of the steps to run
castellatedMesh true;
snap            true;
addLayers       true;


// Geometry. Definition of all surfaces. All surfaces are of class
// searchableSurface.
// Surfaces are used
// - to specify refinement for any mesh cell intersecting it
// - to specify refinement for any mesh cell inside/outside/near
// - to 'snap' the mesh boundary to the surface
geometry
{
    airfoil.stl
    {
        type triSurfaceMesh;
	name airfoil;
    }

    refinementBox
    {
        type searchableBox;
        min ( 0 -2 -1);
        max ( 5  2  1);
    }

    refinementCyl
    {
	type searchableCylinder; 
	point1 (0 0 -1); 
	point2 (0 0  1); 
	radius 2;
    }  

    
    refinementBox1
    {
        type searchableBox;
        min ( 0   -1 -1);
        max ( 2.0  1  1);
    }
    
    refinementCyl1
    {
	type searchableCylinder; 
	point1 (0 0 -1); 
	point2 (0 0  1); 
	radius 1.0;
    }  

    refinementBox2
    {
        type searchableBox;
        min ( 0.4  -0.025 -1);
        max ( 0.8   0.03  1);
    }
    
    refinementCyl2
    {
	type searchableCylinder; 
	point1 (0.605 -0.004 -1); 
	point2 (0.605 -0.004  1); 
	radius 0.005;
    }  
};


// Settings for the castellatedMesh generation.
castellatedMeshControls
{

    include "$FOAM_CONFIG/snappyMesh.castellated";

    // Refinement parameters
    // ~~~~~~~~~~~~~~~~~~~~~

    // Number of buffer layers between different levels.
    // 1 means normal 2:1 refinement restriction, larger means slower
    // refinement.
    nCellsBetweenLevels 9; // C 6 F 8
    

    // Explicit feature edge refinement
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    // Specifies a level for any cell intersected by its edges.
    // This is a featureEdgeMesh, read from constant/triSurface for now.
    features 
    (
//       	{
//             file "airfoil.eMesh";
//             level 6; // C 5 F 7 - should be same as surface refinement
//         }
    );


    // Surface based refinement
    // ~~~~~~~~~~~~~~~~~~~~~~~~

    // Specifies two levels for every surface. The first is the minimum level,
    // every cell intersecting a surface gets refined up to the minimum level.
    // The second level is the maximum level. Cells that 'see' multiple
    // intersections where the intersections make an
    // angle > resolveFeatureAngle get refined up to the maximum level.

    refinementSurfaces
    {
        airfoil
        {
            // Surface-wise min and max refinement level
           level (7 7);    // C 5 F 7
        }
    }


    // Region-wise refinement
    // ~~~~~~~~~~~~~~~~~~~~~~

    // Specifies refinement level for cells in relation to a surface. One of
    // three modes
    // - distance. 'levels' specifies per distance to the surface the
    //   wanted refinement level. The distances need to be specified in
    //   descending order.
    // - inside. 'levels' is only one entry and only the level is used. All
    //   cells inside the surface get refined up to the level. The surface
    //   needs to be closed for this to be possible.
    // - outside. Same but cells outside.

    refinementRegions
    {
        refinementBox
        {
            mode inside;
            levels ((1e15 2)); //C 1  F 2
        }

        refinementCyl
        {
            mode inside;
            levels ((1e15 2)); //C 1  F 2
        }

        refinementBox1
        {
            mode inside;
            levels ((1e15 4)); //C 3 F 4
        }

        refinementCyl1
        {
            mode inside;
            levels ((1e15 4)); //C 3 F 4
        }
        
	refinementBox2
        {
            mode inside;
            levels ((1e15 7)); //C 3 F 4
        }
        
//         refinementCyl2
//         {
//             mode inside;
//             levels ((1e15 8)); //C 3 F 4
//         }
    }


    // Mesh selection
    // ~~~~~~~~~~~~~~
	
    // After refinement patches get added for all refinementSurfaces and
    // all cells intersecting the surfaces get put into these patches. The
    // section reachable from the locationInMesh is kept.
    // NOTE: This point should never be on a face, always inside a cell, even
    // after refinement.
    locationInMesh (-1.0 0 0);

}


// Settings for the snapping.
snapControls
{
    // Per final patch (so not geometry!) the layer information
    featureEdges
    {
    }

    include "$FOAM_CONFIG/snappyMesh.snap";
}


// Settings for the layer addition.
addLayersControls
{
    // Per final patch (so not geometry!) the layer information
    layers
    {
        "airfoil.*"
        {
	    fch                  0.00008; // yPlus = +-100
	    finalLayerThickness  0.5;
            nSurfaceLayers       15;  // C 4  F 8
        }
    }

    include "$FOAM_CONFIG/snappyMesh.layers";
}

include "$FOAM_ETC/dictData/snappyMesh.qualityDict"; // quality metrics
include "$FOAM_ETC/dictData/snappyMesh.misc"; // miscellaneous keywords


// ************************************************************************* //
