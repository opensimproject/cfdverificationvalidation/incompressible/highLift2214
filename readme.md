# CR 2214 closed/clean airfoil

## Domain
   Internal 2214 with chord    c = 0.6096m
   External hexahedral subdomain of size  120 m x 120 m x 0.1 m

## Material
   Polytropic Ideal Gas (PIG)
   Specific heat ratio         gamma = 1.4
   Gas thermodynamic constant  R     = 287.05 J/kgK

## Initial conditions
   Thermodynamic pressure      Poo   = 101325 Pa
   Temperature                 Too   = 288.15 K
   Mach number                 Moo   = 0.2 
   Reynolds number             Re    = 2.83e6
   Angle of attack             alpha = -4 -- 13 deg

## Boundary conditions
   Viscous boundary conditions on internal subdomain
   Riemann boundary conditions on external subdomain

## Turbulence

   Without wall function (y+ < 1)   dy    = +- 0.0000055  
   with wall function (y+ > 30)     dy    = +- 0.00007    
   
   Spalart-Allmaras
   nuTilda (3 x nu)            nuTil = 4.5e-5       (http://turbmodels.larc.nasa.gov/spalart.html)

	k (0.001 x U^2 / Re)        k     = 1.6e-6       (see http://turbmodels.larc.nasa.gov/sst.html)
	w (5 x U / l)               w     = 5.6          ( l is taken as the domain length)

   Viscous wall (No wall function)
   k                           k     = 1e-12 (0)
   w (10*6*nu/(0.075*y^2))     w     = 1e9

## Reference

For more info see
1. NASA CR 2214 - 2D wind-tunnel tests of NASA supercritical airfoil with various high-lift systems ( https://core.ac.uk/download/pdf/42867132.pdf )
2. yPlus calc - http://geolab.larc.nasa.gov/APPS/YPlus/
3. Air properties - http://www.peacesoftware.de/einigewerte/luft_e.html


## Notes

Here follows a few notes on our findings and some of the issues encoutered

1. Farfield
   We ended up using 60 x chords, but we maybe need to investigate the sensitivity of the domain size.

2. Mesh generation
   It was not feasible to create a mesh with the specified domain size and get down to a yPlus of 1 with using just blockMesh and snappy. We therefore opted for a hybrid background mesh created in GMSH which allows us to only do 3 surface ferinements.

3. Wall functions 
   We were able to get solutions from both SA and SST when using wall functions, but they did not compare that well with the experimental data.

4. Numerical results
   The best correlation between numerical and experimental data when using yPlus values of less than 1 so that you do not have to use wall functions. We were able to obtain reliable solutions with SA, but SST had some numerical instability issues that we need to resolve.

5. SimpleFOAM
   Although we were able to obtain reasonable results with simpleFoam, we had to do quite a bit in terms of helping the solver initially.
   * Make sure to do the correct setFields to ensure boundary layer cells ar not initialise with freestream velocities
   * Run with pure 1st order upwind for the first 3000 iterations
   * Need to ensure Cl, Cd and Cm is also converged and not only the flow varibles. Typically required +- 14 K interations. Also need to increase the covergence tolerances on the flow variables as well as the turb variables.
   * Reduce the relaxation factors a bit
  
6. Boundary conditions
   Make sure to use the correct boundary conditions as prescribed on http://turbmodels.larc.nasa.gov/spalart.html and http://turbmodels.larc.nasa.gov/sst.html.
